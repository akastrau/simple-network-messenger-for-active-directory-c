/*
Header file for global functions.

clear() - Clear console output.
Hello_Net() - Display welcome message.
Netword_Discovery() - Function for detecting IP address.
HandlerRoutine() - Handler for window messages (in co-op with WindowProcedure function)
SearchTokenGroupsForSID() - Get a list of user's membership (automatic mode selection - Admin or Client)
pause() - Instead of system("pause")
*/


#ifndef GLOBALFUNCTIONS_H
#define GLOBALFUNCTIONS_H

#include "headers.h"

void clear(void);
void Hello_Net();
void Network_Discovery(char* addr);
BOOL WINAPI HandlerRoutine(DWORD dwCtrlType);
long __stdcall WindowProcedure(HWND window, unsigned int msg, WPARAM wp, LPARAM lp);
void ShowSplashScreen(void);

void NetDiscoverThreadCallback(char* addr);
BOOL SearchTokenGroupsForSID(unsigned int *AccessToken);
void pause(void);

#endif