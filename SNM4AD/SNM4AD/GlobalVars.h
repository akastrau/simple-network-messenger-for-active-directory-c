/* Global structure definition */


#ifndef GLOBALVARS_H
#define GLOBALVARS_H

#include "headers.h"

#define MAX_NAME 256

typedef struct VARIABLES
{
	uv_loop_t *read_loop, *send_loop;
	uv_udp_t read_socket, send_socket;
	uv_udp_send_t send_req;
	uv_buf_t* _msg;
	uv_thread_t thread1, thread2;

	HWND window;
	HINSTANCE hInstance;
	NOTIFYICONDATA nid;
	FLASHWINFO info;

	BOOL thread_run;
	char addr_to_bind[24];
	char addr_to_send[24];
	TCHAR UserName[50];
} GLOBALVARS;



#endif

