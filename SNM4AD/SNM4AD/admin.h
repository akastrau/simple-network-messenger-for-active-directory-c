//Function signature for module admin.c


/*
	Header file for admin module functions.

	Admin is listening on port 1222 UDP
	Sending messages: 1221 UDP

*/

#ifndef ADMIN_H
#define ADMIN_H

void admin_start(char* addr);

#endif

