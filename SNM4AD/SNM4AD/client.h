/*
Header file for client module functions.

Client is listening on port 1221 UDP
Sending messages: 1222 UDP

*/


#ifndef CLIENT_H
#define CLIENT_H

#pragma once
void client_start(char* addr);

#endif