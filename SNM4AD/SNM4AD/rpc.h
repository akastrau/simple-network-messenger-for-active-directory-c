/*
Header file for rpc (Remote procedure calls) module functions.

Remote commands for clients

AVAILABLE COMMANDS: !:reboot, !:shutdown, !:about:me

*/
#pragma once
int check_remote_command(char* command);