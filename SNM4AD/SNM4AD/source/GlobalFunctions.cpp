#pragma once

#include  "../headers.h"
#include "../resource.h"
#include "../GlobalVars.h"

extern GLOBALVARS data;

void clear(void)
{
	COORD topLeft = { 0, 0 };
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO screen;
	DWORD written;

	GetConsoleScreenBufferInfo(console, &screen);
	FillConsoleOutputCharacterA
		(
		console, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written
		);
	FillConsoleOutputAttribute
		(
		console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE,
		screen.dwSize.X * screen.dwSize.Y, topLeft, &written
		);
	SetConsoleCursorPosition(console, topLeft);
}

void Hello_Net()
{
	printf("\n\t\tAdrian Kastrau\n\t\tSimple Network Messenger for Active Directory\n\t\tGdansk University of Technology 2015-2016\n\t=======================================================\n");
}


void Network_Discovery(char* addr)  // "deprecated" way to set address to bind (if u want use a data structure - you're welcome!)
{

	uv_interface_address_t interfacex = {0};
	char buf[25] = { 0 };

	uv_interface_address_t *info = NULL;
	int i = 0;

	uv_interface_addresses(&info, &i);

	while (i--)
	{
		interfacex = info[i];

		if (interfacex.is_internal)
		{

		}
		else
		{
			if (interfacex.address.address4.sin_family == AF_INET)
			{

				uv_ip4_name(&interfacex.address.address4, buf, sizeof(buf));
				if (_strcmpi(interfacex.name, "Ethernet") == 0 || _strcmpi(interfacex.name, "Local Area Connection") == 0 || strcoll(interfacex.name, "Połączenie lokalne") == 0)
				{

					printf("\t\tFound Ethernet interface: %s!\n", buf);
					unsigned ip = interfacex.address.address4.sin_addr.S_un.S_addr;
					unsigned netmask = interfacex.netmask.netmask4.sin_addr.S_un.S_addr;

					unsigned broadcast = ip | (~netmask); //Calculate broadcast address

					char broadcast_addr[INET_ADDRSTRLEN] = { 0 };

					unsigned char bytes[4] = {0};
					bytes[0] = broadcast & 0xFF;
					bytes[1] = (broadcast >> 8) & 0xFF;
					bytes[2] = (broadcast >> 16) & 0xFF;
					bytes[3] = (broadcast >> 24) & 0xFF;
					sprintf_s(broadcast_addr, sizeof(broadcast_addr), "%d.%d.%d.%d\n", bytes[0], bytes[1], bytes[2], bytes[3]);

					//printf("\t\tBroadcast: %s\n\n", broadcast_addr);
					strncpy_s(data.addr_to_bind, strlen(buf) + 1, buf, _TRUNCATE);
					strncpy_s(data.addr_to_send, strlen(buf) + 1, broadcast_addr, _TRUNCATE);
					break;
				}

			}

			printf("\n");

		}

	}

	TCHAR local[50] = { 0 };
	DWORD hstSize = sizeof(local);
	TCHAR UserName[50] = { 0 };
	DWORD UserNameSize = sizeof(UserName);
	GetComputerNameEx(ComputerNameDnsDomain, local, &hstSize);



	if ((_strcmpi(local, "")) == 0)
	{
		printf("\n\n\tComputer is not connected to domain!\n\n");
		GetUserNameA(UserName, &UserNameSize);
		printf("\tHello, %s!\n\n", UserName);
	}

	else
	{
		GetUserNameA(UserName, &UserNameSize);
		printf("\n\n\tUsing Active Directory identity...\n\n\t\tHello, %s@%s!\n\n", UserName, local);
		TCHAR UserNameA[120] = { 0 };
		strncat_s(UserNameA, sizeof(UserNameA), UserName, _TRUNCATE);
		strncat_s(UserNameA, sizeof(UserNameA), "@", _TRUNCATE);
		strncat_s(UserNameA, sizeof(UserNameA), local, _TRUNCATE);
		strncpy_s(data.UserName, strlen(UserNameA) + 1, UserNameA, _TRUNCATE);
	}

	uv_free_interface_addresses(info, i);

}

BOOL WINAPI HandlerRoutine(DWORD dwCtrlType)
{
	switch (dwCtrlType)
	{
	case CTRL_CLOSE_EVENT:
		printf("\n\n\tExitting...\n\n");

		Shell_NotifyIconA(NIM_DELETE, &data.nid);

		Sleep(1000);
		return(TRUE);
	default:
		return (FALSE);
	}
}

void NetDiscoverThreadCallback(char* addr)
{
	Network_Discovery(addr);
}

long __stdcall WindowProcedure(HWND window, unsigned int msg, WPARAM wp, LPARAM lp)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0L;
	default:
		return DefWindowProc(window, msg, wp, lp);
	}
}

void ShowSplashScreen(void)
{
	//Hide Window for Splash Screen
	HWND console = GetConsoleWindow();

	ShowWindow(console, SW_HIDE);

	HWND window = CreateDialogA(0, MAKEINTRESOURCE(IDD_DIALOG1), 0, (DLGPROC)WindowProcedure);
	

	if (window)
	{
		ShowWindow(window, SW_SHOWDEFAULT);
		Sleep(5000);
		DestroyWindow(window);
	}

	ShowWindow(console, SW_RESTORE);
	ShowWindow(console, SW_SHOW);

	FLASHWINFO FlashWindow;

	FlashWindow.cbSize = sizeof(FlashWindow);
	FlashWindow.hwnd = console;
	FlashWindow.dwFlags = FLASHW_ALL;
	FlashWindow.dwTimeout = 500;
	FlashWindow.uCount = 7;

	FlashWindowEx(&FlashWindow);
}

BOOL SearchTokenGroupsForSID(unsigned int *AccessToken) //For Admin AccessToken = 1, for standard user AccessToken = 0
{
	size_t AccessTokenList[2] = { 0 };


	DWORD i, dwSize = 0, dwResult = 0;
	HANDLE hToken;
	PTOKEN_GROUPS pGroupInfo;
	SID_NAME_USE SidType;
	char lpName[MAX_NAME];
	char lpDomain[MAX_NAME];
	PSID pSID = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuth = SECURITY_NT_AUTHORITY;

	// Open a handle to the access token for the calling process.

	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken))
	{
		printf("OpenProcessToken Error %u\n", GetLastError());
		return FALSE;
	}

	// Call GetTokenInformation to get the buffer size.

	if (!GetTokenInformation(hToken, TokenGroups, NULL, dwSize, &dwSize))
	{
		dwResult = GetLastError();
		if (dwResult != ERROR_INSUFFICIENT_BUFFER) {
			printf("GetTokenInformation Error %u\n", dwResult);
			return FALSE;
		}
	}

	// Allocate the buffer.

	pGroupInfo = (PTOKEN_GROUPS)GlobalAlloc(GPTR, dwSize);

	// Call GetTokenInformation again to get the group information.

	if (!GetTokenInformation(hToken, TokenGroups, pGroupInfo,
		dwSize, &dwSize))
	{
		printf("GetTokenInformation Error %u\n", GetLastError());
		return FALSE;
	}

	printf("\n\n\tWe are collecting informations, please wait...\n\n");

	for (i = 0; i < pGroupInfo->GroupCount; i++)
	{
		dwSize = MAX_NAME;
		if (!LookupAccountSid(NULL, pGroupInfo->Groups[i].Sid,
			lpName, &dwSize, lpDomain,
			&dwSize, &SidType))
		{
			dwResult = GetLastError();
			if (dwResult == ERROR_NONE_MAPPED)
				strcpy_s(lpName, dwSize, "NONE_MAPPED");
			else
			{
				printf("\n\n\tLookupAccountSid Error %u\n\n\tMaybe domain controller is down at the moment.\n\tPlease try again!\n\n", GetLastError());
				return FALSE;
			}
		}
	/*	printf("Current user is a member of the %s\\%s group\n",
		lpDomain, lpName);*/

		size_t prompt = 0;

		if (_strcmpi("Enterprise Admins", lpName) == 0 || _strcmpi("Schema Admins", lpName) == 0 || _strcmpi("Domain Admins", lpName) == 0)
		{
			AccessTokenList[1] = 1;
		}

		else if (_strcmpi("Domain Users", lpName) == 0)
		{
			AccessTokenList[0] = 2;
		}

	}

	//printf("AccessTokens: %i %i\n", AccessTokenList[0], AccessTokenList[1]);

	if (AccessTokenList[1] == 1)
	{
		*AccessToken = 1;
	}

	else if (AccessTokenList[0] == 2)
	{
		*AccessToken = 0;
	}

	else
	{
		printf("\n\tError! User must be a member of Domain Admins group\n\tor user must be a member of Domain Users group!\n\n");
	}

	if (pSID)
		FreeSid(pSID);
	if (pGroupInfo)
		GlobalFree(pGroupInfo);
	return TRUE;
}

void pause(void)
{
	printf("Press any key and press enter to continue... ");
	getchar();
}