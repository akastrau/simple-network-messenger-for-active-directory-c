/*
	ADRIAN KASTRAU
	Simple Network Messenger for Active Directory Users and Computers
	Gdansk University of Technology, 2015-2016

	About me page: http://identity-ak-cloud.azurewebsites.net/
	Contact: adrkastr[INSERT 'AT' HERE]student.pg.gda.pl or akastrau[INSERT 'AT' HERE]protonmail.ch

	Extremely simple network messenger, don't look if u don't want.
	At first run, Windows Firewall will prompt you to open two ports 1221/UDP and 1222/UDP. This ports must be opened on firewall before using.

	Feel free to contact me.

	Last one who saw this code died.

THIS SOFTWARE HAS NEVER BEEN AUDITED OR REVIEWED. IT HAS NOT BEEN TESTED. THE AUTHOR IS AMATEUR AND YOU SHOULD NOT USE THIS SOFTWARE FOR ANYTHING IMPORTANT. 
YOU SHOULD NOT RELY ON THE SOFTWARE TO WORK AT ALL, OR IN ANY PREDICTABLE WAY, NOR SHOULD YOU ASSUME THAT THE FEATURES CLAIMED ARE THE FEATURES IMPLEMENTED.
THIS SOFTWARE IS FULL OF ERRORS, THE ARCHITECTURE AND DESIGN ARE BROKEN. UNLESS SOME EXPERT CLAIMS OTHERWISE.

Icon downloaded from: http://www.iconarchive.com/show/windows-8-metro-icons-by-dakirby309/Apps-Email-Chat-Metro-icon.html
Artist: dAKirby309
Iconset: Windows 8 Metro Icons (436 icons)
License: CC Attribution-Noncommercial 4.0
Commercial usage: Not allowed

I used libuv library to send/receive packets to/from clients

*/

/* See .h files for short explanation */

#include "../headers.h"
#include "../admin.h"
#include "../client.h"
#include "../GlobalFunctions.h"
#include "../GlobalVars.h"

GLOBALVARS data = { 0 }; //Global data structure for program


int main()
{
	uv_thread_t NetDiscoverThread;  //Thread for detecting network interfaces
	
	//EnableMenuItem(GetSystemMenu(GetConsoleWindow(), FALSE), SC_CLOSE, MF_DISABLED);

	ShowSplashScreen();

	setlocale(LC_ALL, "");
	
	SetConsoleCtrlHandler(HandlerRoutine, TRUE); //Control handler procedure e.g. exitting from application.

	char addr[25] = { 0 };
	SetConsoleTitleA(TEXT("Simple Network Messenger for Active Directory")); 

	printf("\n\n\tWe are detecting network interfaces settings... please wait!\n\n");

	uv_thread_create(&NetDiscoverThread, (uv_thread_cb)NetDiscoverThreadCallback, addr); //Start a job

	uv_thread_join(&NetDiscoverThread); //Wait for thread.
	
	if (!strlen(data.addr_to_bind))
	{
		printf("\n\n\t\tNo Ethernet devices? Connect a cable and try again...\n\n");
		//system("pause");
		pause();
		return EXIT_FAILURE;
	}

	int ans = 0;
	unsigned int AccessToken = 10;

	SearchTokenGroupsForSID(&AccessToken);

	if (AccessToken == 1)
	{
		printf("\n\n\tEntering admin mode... ");
		Sleep(3000);
		clear();
		Hello_Net();
		admin_start(addr);
	}

	else if (AccessToken == 0)
	{
		printf("\n\n\tEntering client mode... ");
		Sleep(3000);
		clear();
		Hello_Net();
		client_start(addr);
	}

	else
	{
		printf("\n\n\tYou can't use this program without Active Directory Domain!\n\n");
		//system("pause");
		pause();
		return EXIT_FAILURE;
	}

	//For DEBUGGING PURPOSES ONLY!
	
	/*printf("\n\tSelect mode:\n\t1: Admin\n\t2: Client\n\nSelection: ");
	scanf_s("%i", &ans);
	switch (ans)
	{
	case 1:
		clear();
		Hello_Net();
		admin_start(addr);
		break;
	case 2:
		clear();
		Hello_Net();
		client_start(addr);
		break;
	default:
		break;
	}*/


	return EXIT_SUCCESS;
	
}

