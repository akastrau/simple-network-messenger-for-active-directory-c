#pragma once


#include "../headers.h"
#include "../resource.h"
#include "../GlobalVars.h"


void thread_send_C(uv_buf_t*);
extern GLOBALVARS data; //Global data structure

/*
	Function - closing tray.
	Uses SHELLNOTIFYICONDATA structure and Shell_NotifyIconA function to manage application's tray.
*/
void Close_Tray_c(void)
{
	data.nid.cbSize = sizeof(data.nid);
	data.nid.hWnd = data.window;
	data.nid.uFlags = 0;
	Shell_NotifyIconA(NIM_DELETE, &data.nid);
	PostQuitMessage(0);
}

/*

	Creating tray and other cool features.

*/

void Init_windows_cs_Actions_c()
{
	data.window = GetConsoleWindow();
	data.hInstance = GetModuleHandleA(NULL);

	data.nid.uVersion = 4;
	data.nid.cbSize = sizeof(data.nid);
	data.nid.hWnd = data.window;
	data.nid.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE | NIF_INFO | NIF_STATE;
	data.nid.uTimeout = 0;
	data.nid.dwInfoFlags = 0x00000020;

	data.nid.hIcon = (HICON)LoadImage(data.hInstance, MAKEINTRESOURCE(IDI_ICON1), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);

	strcpy_s(data.nid.szTip, ARRAYSIZE(data.nid.szTip), "Simple Network Active Directory Messenger - Client Mode");

	Shell_NotifyIcon(NIM_SETVERSION, &data.nid) ? S_OK : E_FAIL;
	Shell_NotifyIcon(NIM_ADD, &data.nid) ? S_OK : E_FAIL;  //Create tray icon

	data.info.cbSize = sizeof(data.info); //Make window flashing (taskbar)
	data.info.hwnd = data.window;
	data.info.dwFlags = FLASHW_ALL;
	data.info.dwTimeout = 500;
	data.info.uCount = 7;

	
}

/*
	Colorful messages :)

*/

void Change_color_msg_to_blue_C(const uv_buf_t* buf)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	WORD wOldColorAttrs;
	CONSOLE_SCREEN_BUFFER_INFO csbidata;

	GetConsoleScreenBufferInfo(handle, &csbidata);
	wOldColorAttrs = csbidata.wAttributes;

	printf("\n\n\tReceived a message:");

	SetConsoleTextAttribute(handle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);

	printf("\n\n\t%s\n\n", buf->base);

	SetConsoleTextAttribute(handle, wOldColorAttrs);
}

void prepare_message_C(uv_buf_t* buf, int buf_size)
{
	memset(buf->base, 0, buf_size);

	_flushall();

	printf("\n\nEnter a message: ");

	scanf_s("%[^\n]s", buf->base, buf_size);

	_flushall();

	strncat_s(buf->base, buf_size, "\n\n\tMessage from user ", _TRUNCATE);
	strncat_s(buf->base, buf_size, data.UserName, _TRUNCATE);
	strncat_s(buf->base, buf_size, "\n\n\0", _TRUNCATE);

	buf->len = strlen(buf->base) + 1;
}

/*
	Allocating a memory for message
*/

uv_buf_t* message_init_C(uv_udp_send_t *req)
{
	uv_buf_t *buf = (uv_buf_t*)calloc(1, sizeof(uv_buf_t));

	int buf_size = 12512; //Max number of chars to send
	buf->base = new char[buf_size];
	buf->len = buf_size;

	if (buf->base == NULL)
	{
		printf("\n\tCan't allocate memory for message! :(\n");
		buf->len = 0;
		return buf;
	}

	return buf;
}


/*
	Allocating memory for incoming message
*/

void alloc_buffer_cb_C(uv_handle_t *handle, size_t size, uv_buf_t *buf)
{
	buf->base = new char[size];
	buf->len = size;

	assert(buf->base != NULL);
}

/*
	On receive message callback

	Checks if message is a remote command. If not displays the message on screen
*/

void on_recv_cb_C(uv_udp_t *req, ssize_t nread, const uv_buf_t *buf, const struct sockaddr *addr, unsigned int flags)
{

	if (nread < 0)
	{
		printf("An unexpected receive error has been occurred!\n");
		uv_close((uv_handle_t*)req, NULL);
		free(buf->base);
		return;
	}

	if ((strcmp(buf->base, data._msg->base)) == 0)
	{

		free(buf->base);
		return;
	}

	int test = -1;
	test = check_remote_command(buf->base);

	if (test >=0)
	{
		
	}

	else
	{
		strcpy_s(data.nid.szInfo, ARRAYSIZE(data.nid.szInfo), "Administrator messaged to you!");
		strcpy_s(data.nid.szInfoTitle, ARRAYSIZE(data.nid.szInfoTitle), "Information");


		Shell_NotifyIcon(NIM_MODIFY, &data.nid) ? S_OK : E_FAIL;


		ShowWindow(data.window, SW_RESTORE);
		UpdateWindow(data.window);
		FlashWindowEx(&data.info);



		Change_color_msg_to_blue_C(buf);

		if (data.thread_run)
		{
			printf("\n\nEnter a message: ");
		}
	}
	


	free(buf->base);
	//uv_udp_recv_stop(req);
}

/*
		Send callback function
*/

void on_send_cb_C(uv_udp_send_t *req, int status)
{
	if (status) error("Something went wrong... I couldn't send a message!", status);
	if (!status) printf("\n\tMessage has been sent!\n");

}

/*
		Start listening on port 1221/UDP

*/

void start_recv_C()
{

	int test;

	struct sockaddr_in recv_addr;
	uv_ip4_addr("0.0.0.0", 1221, &recv_addr);
	test = uv_udp_bind(&data.read_socket, (const struct sockaddr *)&recv_addr, UV_UDP_REUSEADDR);

	if (test)
	{
		printf("\n\n\tCan't listen for connections!\n\n");
	}

	uv_udp_recv_start(&data.read_socket, alloc_buffer_cb_C, on_recv_cb_C);

}

/*
	Function for sending a message.

*/

void send_msg_C(uv_buf_t *message)
{
	int test;
	struct sockaddr_in broadcast_addr;

	//Quit if message is NULL

	if (message->base == NULL || message->len <= 1)
	{
		printf("\n\tCan't send a message! MESSAGE ERROR!\n\n");
		uv_thread_create(&data.thread2, (uv_thread_cb)thread_send_C, data._msg);

	}

	else
	{
		while (data.send_socket.send_queue_size > 1 || data.send_socket.send_queue_count == 1)
		{
			printf("\n\n\tIt seems that our send queue is in deadlock right now... :(\n\n");
			Sleep(1500);
			uv_run(data.send_loop, UV_RUN_DEFAULT);
		}

		//set broadcast addr;

		test = uv_ip4_addr(data.addr_to_bind, 0, &broadcast_addr);
		if (test) error("Error while setting broadcast address! Additional data.info: ip4_addr", test);

		//  init, bind, broadcast
		uv_udp_init(data.send_loop, &data.send_socket);
		uv_udp_bind(&data.send_socket, (const struct sockaddr*)&broadcast_addr, 0);
		uv_udp_set_broadcast(&data.send_socket, 1);

		struct sockaddr_in send_addr;
		uv_ip4_addr(data.addr_to_send, 1222, &send_addr);

		test = uv_udp_send(&data.send_req, &data.send_socket, message, 1, (const struct sockaddr *)&send_addr, on_send_cb_C);

		if (test)
		{
			printf("Error! Can't send message!\n");

		}

		uv_run(data.send_loop, UV_RUN_DEFAULT);

		uv_thread_create(&data.thread2, (uv_thread_cb)thread_send_C, data._msg);

	}


}

/*
	Thread for listening daemon

*/

void thread_read_C(void *arg)
{
	start_recv_C();
}


/*
	Thread for sender daemon

	Max length of message: 12512 chars
*/
void thread_send_C(uv_buf_t* _msg)
{
	data.thread_run = true;

	prepare_message_C(_msg, 12512);

	send_msg_C(_msg);

	data.thread_run = false;

}

/*
	Start Client functionality

*/

void client_start(char* addr)
{
	printf("\t\t\tClient Mode\n");

	SetConsoleTitleA(TEXT("Simple Network Messenger for Active Directory - Client Mode"));

	data.send_loop = (uv_loop_t*)calloc(1, sizeof(uv_loop_t));
	uv_loop_init(data.send_loop);

	Init_windows_cs_Actions_c();

	data.read_loop = (uv_loop_t*)calloc(1, sizeof(uv_loop_t));
	uv_loop_init(data.read_loop);

	uv_udp_init(data.read_loop, &data.read_socket);

	uv_thread_create(&data.thread1, thread_read_C, 0);

	uv_thread_join(&data.thread1);

	data._msg = message_init_C(&data.send_req);


	uv_thread_create(&data.thread2, (uv_thread_cb)thread_send_C, data._msg);

	atexit(Close_Tray_c);

	uv_run(data.read_loop, UV_RUN_DEFAULT);
}