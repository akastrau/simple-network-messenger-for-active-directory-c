#pragma once

#include "string.h"
#include "../headers.h"

void do_reboot()
{
	HINSTANCE r = ShellExecute(NULL, "open", "shutdown.exe", "/r /t 60  /c \"Your computer is going to reboot in 1 minute. Best regards. Network Administrator\"", NULL, SW_HIDE);
}
void do_shutdown()
{
	HINSTANCE r = ShellExecute(NULL, "open", "shutdown.exe", "/s /t 60  /c \"Your computer is going to shutdown in 1 minute. Best regards. Network Administrator\"", NULL, SW_HIDE);
}

void show_about_me_page()
{
	HINSTANCE r = ShellExecute(NULL, "open", "http://identity-ak-cloud.azurewebsites.net", NULL, NULL, SW_SHOWNORMAL);

}

int check_remote_command(char* command)
{
	char commands[][12] = { "!:reboot", "!:shutdown", "!:about:me" };



	for (size_t i = 0; i < 3; i++)
	{
		if (!memcmp(commands[i], command, 12))
		{
			switch (i)
			{
			case 0:
				do_reboot();
				return i;
			case 1:
				do_shutdown();
				return i;
			case 2:
				show_about_me_page();
				return i;
			default:
				break;
			}
		}
	}

	return -1;
}